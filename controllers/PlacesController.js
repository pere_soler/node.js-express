const Place = require('../models/Place')

function index(req,res){
  //todos los registros
  Place.find({}).then(docs=>{
    res.json(docs)
  }).catch(err=>{
    console.log(err)
    res.json(err)
  })
}

function create(req, res){
  //crear registros
  Place.create({
    title: req.body.title,
    description:req.body.description,
    acceptsCreditCard:req.body.acceptsCreditCard,
    openHours:req.body.openHours,
    closeHours:req.body.closeHours
  }).then(doc=>{
    res.json(doc)
  }).catch(err=>{
    console.log(err)
    res.json(err)
  })
}

function show(req, res){
  //busqueda individual
  Place.findById(req.params.id)
  .then(doc=>{
    res.json(doc)
  .catch(err=>{
    console.log(err)
    res.json(err)
  })
  })
}

function update(req, res){
  //modificar registros
  let atributes = ['title', 'description', 'acceptsCreditCard', 'openHours', 'closeHours']
  let placeParams = {}
  atributes.forEach(attr =>{
    if(Object.prototype.hasOwnProperty.call(req.body,attr))
      placeParams[attr] = req.body[attr]
  })

  Place.update({'_id': req.params.id}, placeParams).then(doc=>{
    res.json(doc)
  }).catch(err=>{
    console.log(err)
    res.json(err)
  })
}

function destroy(req, res){
  //borrar registros
  Place.findByIdAndRemove(req.params.id)
    .then(doc=>{
      res.json({})
    })
    .catch(err=>{
      console.log(err)
      res.json(err)
    })
}

module.exports = {index, create, show, update, destroy};
