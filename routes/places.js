var express = require('express');
var router = express.Router();


const placeController = require('../controllers/PlacesController')

router.route('/')
  .get(placeController.index)
  .post(placeController.create)



router.route('/:id')
  .get(placeController.show)
  .put(placeController.update)
  .delete(placeController.destroy)



module.exports = router;
